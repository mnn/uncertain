.. uncertain documentation master file, created by
   sphinx-quickstart on Tue Apr  5 21:47:46 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to uncertain's documentation!
=====================================

Python module to keep track of uncertainties in mathematical calculations.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   uncertain
   examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
